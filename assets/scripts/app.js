/**
 * CLASS LIST
 * ProjectList
 * ProjectItem
 * App
 * Tooltip
 */

class DOMHelper{

    static clearEventListeners(element) {
        const clonedElement = element.cloneNode(true);
        element.replaceWith(clonedElement);
        return clonedElement;
    }

    static moveElement(elementId, newDestinationSelector) {
        const element = document.getElementById(elementId);
        const destinationElement = document.querySelector(newDestinationSelector);
        destinationElement.append(element);
    }
}

class Tooltip{

    constructor(close) {
        this.close = close;
    }

    closeTooltip = () => {
        this.detach();
        this.close();
    }

    detach = () => {
        this.element.remove();
    }

    show = () => {
        const tooltipElement = document.createElement('div');
        tooltipElement.classList.add('card');
        tooltipElement.textContent = 'DUMMY!';
        tooltipElement.addEventListener('click', this.closeTooltip);
        this.element = tooltipElement;
        document.body.append(tooltipElement);
    }
}

class ProjectItem{

    hasActiveTooltip = false;

    constructor(id, updateProjectLists, type) {
        this.id = id;
        this.updateProjectLists = updateProjectLists;
        this.connectMoreInfoButton();
        this.connectSwitchButton(type);

    }

    showMoreInfo = () => {
        if (this.hasActiveTooltip) return;
        const tooltip = new Tooltip(() => this.hasActiveTooltip === false);
        tooltip.show();
        this.hasActiveTooltip = true;
    }

    connectMoreInfoButton = () => {
        const projectItemElement = document.getElementById(this.id);
        const moreInfoBtn = projectItemElement.querySelector('button:first-of-type');
        moreInfoBtn.addEventListener('click', this.showMoreInfo)
    }

    connectSwitchButton = (type) => {
        const projectItemElement = document.getElementById(this.id);
        let switchBtn = projectItemElement.querySelector('button:last-of-type');
        switchBtn = DOMHelper.clearEventListeners(switchBtn);
        switchBtn.textContent = type === 'active' ? 'Finish' : 'Activate';
        switchBtn.addEventListener('click', () => this.updateProjectLists(this.id));
    }

    update(updateProjectListFn, type) {
        this.updateProjectLists = updateProjectListFn;
        this.connectSwitchButton(type);
    }

}

class ProjectList{
    projects = [];

    constructor(type) {
        this.type = type;
        const prjItems = document.querySelectorAll(`#${type}-projects li`);
        for (const item of prjItems) {
            this.projects.push(new ProjectItem(item.id, this.switchProject, this.type));
        }
        console.log(this.projects);
    }

    setSwitchHandlerFunction = (switchHandlerFunction) => {
        this.switchHandler = switchHandlerFunction;
    }

    addProject = (project) => {
        this.projects.push(project);
        DOMHelper.moveElement(project.id, `#${this.type}-projects ul`);
        project.update(this.switchProject, this.type);
    }

    switchProject = (projectId) => {
        this.switchHandler(this.projects.find(p => p.id === projectId));
        this.projects = this.projects.filter(p => p.id !== projectId);
    }
}

class App{
    static init() {
        const activeProjectsList = new ProjectList('active');
        const finishedProjectsList = new ProjectList('finished');

        activeProjectsList.setSwitchHandlerFunction(finishedProjectsList.addProject)
        finishedProjectsList.setSwitchHandlerFunction(activeProjectsList.addProject)
    }
}
App.init();